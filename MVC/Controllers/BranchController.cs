﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVC.Models;
using MVC.DAL;
using Newtonsoft.Json;
using System.Collections;

namespace MVC.Controllers
{
    public class BranchController : Controller
    {
        private ShopContext db = new ShopContext();

        private static List<DataPoint> _branchesDataPoints = new List<DataPoint>();
        private static List<DataPoint> _customersDataPoints = new List<DataPoint>();
        private static List<DataPoint> _ordersDataPoints = new List<DataPoint>();

        //
        // GET: /Branch/

        public ActionResult Index()
        {
            ViewBag.City = new SelectList(db.Branches.Select(x => x.City).Distinct());

            ViewBag.BranchID = new SelectList(db.Branches, "BranchID", "Name");
            return View(db.Branches.ToList());
        }

        [HttpPost]
        public ActionResult Index(string txt, int? BranchID, string City)
        {

            var branches = from b in db.Branches select b;

            if (!string.IsNullOrEmpty(txt))
            {
                branches = branches.Where(x => x.Name.Contains(txt));
            }
            if (BranchID != null)
            {
                branches = branches.Where(x => x.BranchID == BranchID);
            }
            if (!string.IsNullOrEmpty(City))
            {
                branches = branches.Where(x => x.City == City);
            }

            ViewBag.BranchID = new SelectList(db.Branches, "BranchID", "Name");
            ViewBag.City = new SelectList(db.Branches.Select(x => x.City).Distinct());

            return View(branches);
            
            
        }

        //
        // GET: /Branch/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Branch/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Branch branch)
        {
            if (ModelState.IsValid)
            {
                db.Branches.Add(branch);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(branch);
        }

        //
        // GET: /Branch/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Branch branch = db.Branches.Find(id);
            if (branch == null)
            {
                return HttpNotFound();
            }
            return View(branch);
        }

        //
        // POST: /Branch/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Branch branch)
        {
            if (ModelState.IsValid)
            {
                db.Entry(branch).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(branch);
        }

        public JsonResult Delete(int id)
        {
            Branch branch = db.Branches.Find(id);
            db.Branches.Remove(branch);
            db.SaveChanges();
            return Json(id);
        }

        public ActionResult Chart()
        {
            _branchesDataPoints = getDataBranchesForChart();
            _customersDataPoints = getDataCustomersForChart();
            _ordersDataPoints = getDataOrdersForChart();
            ViewBag.BranchesDataPoints = JsonConvert.SerializeObject(_branchesDataPoints);
            ViewBag.CostumersDataPoints = JsonConvert.SerializeObject(_customersDataPoints);
            ViewBag.OredersDataPoints = JsonConvert.SerializeObject(_ordersDataPoints);
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }


        private List<DataPoint> getDataCustomersForChart()
        {
            var Customersdatapoints = db.Customers.GroupBy(b => b.City).Select(grp => new { grp.Key, Count = grp.Count() }).ToList();
            List<DataPoint> datapointsList = new List<DataPoint>();
            foreach (var dCurr in Customersdatapoints)
            {
                datapointsList.Add(new DataPoint(dCurr.Key ?? "", dCurr.Count ));
            }
            return datapointsList;
        }

        private List<DataPoint> getDataOrdersForChart()
        {
            var orderPerCustomerDatapoints = from c in db.Customers
                                   join o in db.Orders on c.CustomerID equals o.CustomerID
                                   group c by c.CustomerID into g
                                   select new { Key = g.Key.ToString(), Count = g.Count() };
            List<DataPoint> datapointsList = new List<DataPoint>();
            foreach (var dCurr in orderPerCustomerDatapoints)
            {
                datapointsList.Add(new DataPoint(dCurr.Key ?? "", dCurr.Count));
            }
            return datapointsList;
        }

        private List<DataPoint> getDataBranchesForChart()
        {
            var branchDatapoints = db.Branches.GroupBy(b => b.City).Select(grp => new { grp.Key, Count = grp.Count() }).ToList();
            List<DataPoint> datapointsList = new List<DataPoint>();
            foreach (var dCurr in branchDatapoints)
            {
                datapointsList.Add(new DataPoint(dCurr.Key ?? "", dCurr.Count));
            }
            return datapointsList;
        }

        JsonSerializerSettings _jsonSetting = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };
    }
}